#include <avr/io.h>
#define F_CPU 2000000UL
#include <util/delay.h> 
#include <avr/interrupt.h>

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

#define RST 0x69//'i'
#define left 0x6C//'l'
#define right 0x72//'r'
#define junk 0x6A//j

#define ADC0 0x05
#define ADC1 1

#define lakn 0x61//a
#define rakn 0x73//'s'

volatile char ReceivedByte;
int li,ri;
void init_adc(int n)
{
	
	ADMUX=n;
	ADMUX|=(1<<REFS1)|(1<<REFS0);
	ADMUX|=(1<<ADLAR);
	ADCSRA|=(1<<ADPS2)|(1<<ADPS0);
	ADCSRA|=(1<<ADEN);
}
unsigned char read_adc(int n)
{
	
	init_adc(n);
	int I;
	ADCSRA|=_BV(ADSC);
	while(!(ADCSRA & 0x10));
	I=ADCH;
	ADCSRA&=~(1<<ADEN);
	return I;
}
void transmit(int data)
{
	while(!(UCSRA & (1<<UDRE)));
	UDR=data;

}
int main(void)
{
	DDRB|=(1<<0)|(1<<1);
	DDRB&=~((1<<2)|(1<<3));
	PORTB=0;
	DDRC|=(1<<2)|(1<<3);
	PORTC&=~((1<<2)|(1<<3));
	unsigned char rr,ll;
	//\\\\\\\\\\\\\USART INITIALIZATION////////////////////
	UCSRB = (1 << TXEN);   
    UCSRC = (1 << UCSZ0) | (1 << UCSZ1); 
    UBRRH = (BAUD_PRESCALE >> 8); 
    UBRRL = BAUD_PRESCALE; 
    sei(); 		
    while(1)
	{
	PORTC|=(1<<2)|(1<<3);
	_delay_ms(100);
	ll=read_adc(ADC0);
    _delay_ms(100);
	rr=read_adc(ADC1);
	PORTC&=~((1<<2)|(1<<3));
	if (ll>rr)
	{
		if ((ll-rr)<30)
			transmit(junk);
		else
			transmit(right);		
	}
	else if (ll<rr)
	{
		if ((rr-ll)<30)
			transmit(junk);
		else
			transmit(left);
	}
	_delay_ms(500);	
	}

}

