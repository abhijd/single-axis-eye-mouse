# Single Axis Computer Cursor Movement

As the name suggests, the aim of the project is to control the cursor of the computer using only the movement of the eye. Usually the cursor is controlled by mouse or touchpad which needs the use of hand and fingers. The development of this project allows any user to control the cursor with only eye, keeping the hands free for other tasks. This project may also help people without hands to be able to use computers and be self-dependent. 

To sense the movement of the pupils we used infra-red (IR) receiver and transmitter. IR is reflected from the eye and received by the receiver. The black pupil of the eye absorbs more amount of IR than the white part. So the movement can be detected from the difference of the receiver voltage.

This Project is just a prototype that was supposed to be developed in iteration into a complete product. In this first prototype phase, only single axis cursor movement is implemented using IR sensor. Using cameras and DSP multi axis movement can be implemented with options to click. As the equipment cost was on students, us, we only used economical components like inexpensive IR transmitter and receiver to keep the cost affordable for our student wallet. 


## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Proteus Design Suite


### How-to-simulate
 
 * Clone the repo in your local directory
 * Start Proteus Design Suite
 * In Proteus, open "proj_mega8.DSN" 
 * When the design opens, double-click on the Atmega8 microcontroller that opens the "edit component" pop-up
 * From the edit component, add the location of the fresh.hex file (located in the folder ./fresh/default/fresh.hex) in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!
 * Once started, drag and change the RV1 and RV2 to imitate the movement of the eye.
 

### Simulation Schematics

![Simulation Schematics](simualtion-schematics.png "Simulation Schematics")
 
## Changing the program code

  * To change the program code, you may use AVR Studio/WinAVR to open the fresh.c file (located in the folder solar_tracker_with avr) and change as desired.
  * Once changed, compile the hex file again using AVR Studio/ WinAVR, and load it again to the Atmega8 following the steps from "How-to-simulate"
  * If you are using AVR Studio you may use the Makefile in the folder solar_tracker_with avr to quickly configure the compiler. 
  
## Hardware Implementation 

  * A simple voltage regulator circuit should be used to power the microcontroller
  * 
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The main.hex file must me burned (not literally) in the Atmega8 microcontroller using AVR programmer or some universal programmer that supports it.
  
### Hardware Schematics

![Hardware Schematics](hardware-schematics.png "Hardware Schematics")

### Hardware Diagram

![Hardware Diagram](hardware-assembled.jpg "Hardware Diagram")
 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* I am thankful to Tanvir Hasan for writing the VB application and Anwarul for going to the end of the world to buy some of the project hardware components.
* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

